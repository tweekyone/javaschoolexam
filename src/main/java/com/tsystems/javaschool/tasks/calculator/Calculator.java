package com.tsystems.javaschool.tasks.calculator;

import java.text.DecimalFormat;
import java.util.*;

public class Calculator {

    /**
     * Evaluate statement represented as string.
     *
     * @param statement mathematical statement containing digits, '.' (dot) as decimal mark,
     * parentheses, operations signs '+', '-', '*', '/'<br>
     * Example: <code>(1 + 38) * 4.5 - 1 / 2.</code>
     * @return string value containing result of evaluation or null if statement is invalid
     */
    private static final String OPERATORS = "+-*/^";

    private LinkedList<String> outputRPN;
    private Stack<String> operations;

    public String evaluate(String statement) {
        outputRPN = new LinkedList<>();
        operations = new Stack<>();
        if (statement != null && !statement.isEmpty()) {
            StringTokenizer tokenizer = new StringTokenizer(statement, OPERATORS + "()", true);
            while (tokenizer.hasMoreTokens()) {
                if (!shuntingYard(tokenizer.nextToken())) {
                    return null;
                }
            }
            while (!operations.isEmpty()) {
                if (operations.lastElement().equals("(") || operations.lastElement().equals(")")) {
                    return null;
                }
                outputRPN.add(operations.pop());
            }

            return calc();
        }
        return null;
    }

    public boolean isNumber(String token) {
        try {
            Double.parseDouble(token);
        } catch (Exception e) {
            return false;
        }
        return true;
    }

    private byte getPrecedence(String token) {
        switch (token) {
            case "+":
                return 1;
            case "-":
                return 2;
            case "*":
                return 3;
            case "/":
                return 4;
            case "^":
                return 5;
            default:
                return 0;
        }
    }

//    public int getPrecedence(String token) {
//        if (token.equals("+") || token.equals("-")) {
//            return 1;
//        }
//        return 2;
//    }

    public boolean shuntingYard(String token) {
        if (isNumber(token)) {
            outputRPN.add(token);
            return true;
        } else if (OPERATORS.contains(token)) {
            while (!operations.empty()
                    && OPERATORS.contains(operations.lastElement())
                    && getPrecedence(token) <= getPrecedence(operations.lastElement())) {
                outputRPN.add(operations.pop());
            }
            operations.push(token);
            return true;
        } else if (token.equals("(")) {
            operations.push(token);
            return true;
        } else if (token.equals(")")) {
            if (operations.isEmpty() || !operations.contains("(")) {
                return false;
            }
            while (!operations.empty() && !operations.lastElement().equals("(")) {
                outputRPN.add(operations.pop());
            }
            operations.pop();
            return true;
        }
        return false;
    }

    private String calc() {
        Stack<Double> stack = new Stack<>();
        if (!outputRPN.isEmpty()) {
            for (String element : outputRPN) {
                try {
                    switch (element) {
                        case ("+"):
                            stack.push(stack.pop() + stack.pop());
                            break;
                        case ("-"):
                            Double b = stack.pop();
                            Double a = stack.pop();
                            stack.push(a - b);
                            break;
                        case ("*"):
                            stack.push(stack.pop() * stack.pop());
                            break;
                        case ("/"):
                            Double d = stack.pop();
                            Double c = stack.pop();
                            if (d != 0) {
                                stack.push(c / d);
                            } else {
                                return null;
                            }
                            break;
                        case ("^"):
                            Double f = stack.pop();
                            Double e = stack.pop();
                            stack.push(Math.pow(e, f));
                            break;
                        default:
                            stack.push(Double.parseDouble(element));
                            break;
                    }
                } catch (EmptyStackException e) {
                    return null;
                }
            }
        }
        DecimalFormat decimalFormat = new DecimalFormat("#.####");
        return decimalFormat.format(stack.pop()).replace(",", ".");
    }
}
