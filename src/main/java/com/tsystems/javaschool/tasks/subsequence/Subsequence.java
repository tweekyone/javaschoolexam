package com.tsystems.javaschool.tasks.subsequence;

import java.util.ArrayList;
import java.util.LinkedList;
import java.util.List;
import java.util.Objects;

public class Subsequence {

    /**
     * Checks if it is possible to get a sequence which is equal to the first
     * one by removing some elements from the second one.
     *
     * @param x first sequence
     * @param y second sequence
     * @return <code>true</code> if possible, otherwise <code>false</code>
     */
    @SuppressWarnings("rawtypes")
    public boolean find(List x, List y) {
        if (x == null || y == null) {
            throw new IllegalArgumentException();
        }

        if ((x.isEmpty() && !y.isEmpty()) || (x.isEmpty() && y.isEmpty())) {
            return true;
        } else if (y.isEmpty() && !x.isEmpty()) {
            return false;
        }

        ArrayList<Object> listX = new ArrayList<>(x);
        ArrayList<Object> listY = new ArrayList<>(y);

        ArrayList<Object> resultList = new ArrayList<>();

        int lastY = 0;
        for (int i = 0; i < listX.size(); i++) {
            for (int j = lastY; j < listY.size(); j++) {
                if (Objects.equals(listX.get(i), listY.get(j))) {
                    resultList.add(listY.get(j));
                    lastY = j;
                    break;
                }
            }
        }

        if (Objects.equals(listX, resultList)) return true;

        return false;
    }
}
