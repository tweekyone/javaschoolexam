package com.tsystems.javaschool.tasks.pyramid;

import java.util.Collections;
import java.util.LinkedList;
import java.util.List;

public class PyramidBuilder {

    /**
     * Builds a pyramid with sorted values (with minumum value at the top line and maximum at the bottom,
     * from left to right). All vacant positions in the array are zeros.
     *
     * @param inputNumbers to be used in the pyramid
     * @return 2d array with pyramid inside
     * @throws {@link CannotBuildPyramidException} if the pyramid cannot be build with given input
     */
    public int[][] buildPyramid(List<Integer> inputNumbers) {
        if (inputNumbers.size() >= 255) {
            throw new CannotBuildPyramidException();
        }
        LinkedList<Integer> sortedList = new LinkedList<>(inputNumbers);
        try {
            Collections.sort(sortedList);
        } catch (NullPointerException e) {
            throw new CannotBuildPyramidException();
        }

        int rows = gerNumberOfRows(sortedList.size());

        int[][] result = new int[rows][rows + rows - 1];
        int arrayIndex = 0;
        for (int i = result.length - 1; i >= 0; i--) {
            for (int j = result[0].length - 1; j >= 0; j -= 2) {
                if (j - arrayIndex >= 0 && j - arrayIndex >= arrayIndex) {
                    try {
                        result[i][j - arrayIndex] = sortedList.pollLast();
                    } catch (NullPointerException e) {
                        throw new CannotBuildPyramidException();
                    }
                } else {
                    break;
                }
            }
            arrayIndex++;
        }
        return result;
    }

    public int gerNumberOfRows(int inputNumbersSize) {
        int numbers = 0,
                rows = 0,
                index = 1;
        while (numbers < inputNumbersSize) {
            numbers += index++;
            rows++;
        }
        return rows;
    }
}
